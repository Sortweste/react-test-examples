import { rest } from 'msw';

import mockResponse from './mockResponse';

const { REACT_APP_API_BASE_URL: baseURL } = process.env;

const handlers = [
  rest.get(`${baseURL}pokemon/ditto`, (req, res, ctx) =>
    mockResponse(req, res, ctx)
  ),
  rest.get('*', (req, res, ctx) => {
    return res(ctx.status(500), ctx.json({ error: 'Internal Server Error' }));
  }),
];

export default handlers;
