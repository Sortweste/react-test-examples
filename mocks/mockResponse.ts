import {
  DefaultRequestBody,
  RequestParams,
  ResponseResolver,
  RestContext,
  RestRequest,
} from 'msw';

type MockResponseT = ResponseResolver<
  RestRequest<DefaultRequestBody, RequestParams>,
  RestContext,
  any
>;

const mockResponse: MockResponseT = (req, res, ctx) => {
  return res(
    ctx.status(200),
    ctx.json({
      data: {
        name: 'Ditto',
        number: 132,
        ability: 'Limber',
        move: 'Transform',
      },
    })
  );
};

export default mockResponse;
