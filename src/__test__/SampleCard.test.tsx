import { fireEvent, render } from '@testing-library/react';

import SampleCard from 'components/SampleCard';

test('should render SampleCard Component', () => {
  const { getByAltText } = render(<SampleCard />);
  expect(getByAltText(/pokemon/i)).toBeTruthy();
});

test('should display SampleCard Title', () => {
  const { getByText } = render(<SampleCard />);
  expect(getByText(/Bulbasaur/i)).toBeTruthy();
});

test('should change text onClick event', () => {
  const { getByText, queryByText } = render(<SampleCard />);
  expect(queryByText(/less of its HP remaining/i)).toBeInTheDocument();
  fireEvent.click(getByText(/More information/i), { button: 0 });
  expect(queryByText(/less of its HP remaining/i)).not.toBeInTheDocument();
});
