import { fireEvent, screen } from '@testing-library/dom';

import Navbar from 'components/Navbar';

import renderWithWrapper from './utils/Wrapper';

test('should be in home page', () => {
  renderWithWrapper(<Navbar />);
  expect(screen.getByTestId('location-display')).toHaveTextContent('/');
});

test('should navigate to form page', () => {
  const { getByText } = renderWithWrapper(<Navbar />);
  fireEvent.click(getByText(/Form/i), { button: 0 });
  expect(screen.getByTestId('location-display')).toHaveTextContent('/form');
});

test('should navigate to pokedex page', () => {
  const { getByText } = renderWithWrapper(<Navbar />);
  fireEvent.click(getByText(/Pokedex/i), { button: 0 });
  expect(screen.getByTestId('location-display')).toHaveTextContent('/pokedex');
});
