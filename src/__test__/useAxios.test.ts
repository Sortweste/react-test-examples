import { renderHook } from '@testing-library/react-hooks';

import useAxios from 'hooks/useAxios';

const mockAxiosRequestConfig = jest.fn();

const { REACT_APP_API_BASE_URL: baseURL } = process.env;

test('should make API Request', async () => {
  mockAxiosRequestConfig.mockReturnValue({
    url: `${baseURL}pokemon/ditto`,
  });

  const { result, waitForNextUpdate } = renderHook(() =>
    useAxios<any>(mockAxiosRequestConfig())
  );

  expect(result.current[0].isLoading).toEqual(true);
  await waitForNextUpdate();
  const { isLoading, isSuccess, data } = result.current[0];
  expect(isLoading).toEqual(false);
  expect(isSuccess).toEqual(true);
  expect(data.ability).toBe('Limber');
});
