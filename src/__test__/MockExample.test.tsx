import { fireEvent, render } from '@testing-library/react';

import Button from 'components/Button';

type ExamplePropsT = {
  someFunction: () => void;
};

const Example = ({ someFunction }: ExamplePropsT) => {
  return <Button text="Click Me" onClick={someFunction} />;
};

test('function must be called on click', () => {
  const mockFunction = jest.fn();
  const { getByText } = render(<Example someFunction={mockFunction} />);
  fireEvent.click(getByText(/Click Me/i), { click: 0 });
  expect(mockFunction).toBeCalledTimes(1);
});
