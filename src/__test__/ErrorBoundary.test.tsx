import { FunctionComponent, ReactNode } from 'react';

import { render } from '@testing-library/react';

import ErrorBoundary from 'components/ErrorBoundary';

type BombPropsT = {
  shouldThrow?: boolean;
};

const Bomb = ({ shouldThrow = false }: BombPropsT) => {
  if (shouldThrow) {
    throw new Error('💣');
  } else {
    return null;
  }
};

type WrapperPropsT = {
  children: ReactNode;
};

const Wrapper = ({ children }: WrapperPropsT) => (
  <ErrorBoundary>{children}</ErrorBoundary>
);

beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});

afterAll(() => {
  jest.restoreAllMocks();
});

afterEach(() => {
  jest.clearAllMocks();
});

// jest.mock('someFunction');

/**
 const props = {
  onConfirm: jest.fn()
 };
*/

test('renders that there was a problem', () => {
  const { rerender, getByText } = render(<Bomb />, {
    wrapper: Wrapper as FunctionComponent,
  });

  rerender(<Bomb shouldThrow />);

  expect(getByText('💣')).not.toBe(null);
});
