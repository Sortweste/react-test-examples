import { screen, waitFor } from '@testing-library/react';

import App from 'App';

import renderWithWrapper from './utils/Wrapper';

test('should display Not Found Page when URL is not defined', async () => {
  renderWithWrapper(<App />, '/invalidURL');
  await waitFor(() => expect(screen.getByText(/404/i)).toBeInTheDocument());
});
