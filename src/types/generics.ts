export interface IFetchState<T> {
  isLoading: boolean;
  isError?: boolean;
  isSuccess: boolean;
  error?: string;
  data?: T;
}

export interface IResponse<T> {
  data: T;
}
