import FetchActionTypes from './fetchDataTypes';

type FetchDataActions<T> =
  | { type: FetchActionTypes.FETCH_DATA_INIT }
  | { type: FetchActionTypes.FETCH_DATA_SUCCESS; payload: T }
  | { type: FetchActionTypes.FETCH_DATA_FAILURE; payload: string };

const onFetchDataInitCreator = <T>(): FetchDataActions<T> => {
  return { type: FetchActionTypes.FETCH_DATA_INIT };
};

const onFetchDataSuccessCreator = <T>(payload: T): FetchDataActions<T> => {
  return { type: FetchActionTypes.FETCH_DATA_SUCCESS, payload };
};

const onFetchDataFailureCreator = <T>(payload: string): FetchDataActions<T> => {
  return { type: FetchActionTypes.FETCH_DATA_FAILURE, payload };
};

export default FetchDataActions;

export {
  onFetchDataInitCreator,
  onFetchDataSuccessCreator,
  onFetchDataFailureCreator,
};
