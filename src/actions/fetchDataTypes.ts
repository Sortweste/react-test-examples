enum FetchActionTypes {
  'FETCH_DATA_INIT',
  'FETCH_DATA_SUCCESS',
  'FETCH_DATA_FAILURE',
}

export default FetchActionTypes;
