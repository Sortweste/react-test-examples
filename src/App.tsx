import ErrorBoundary from 'components/ErrorBoundary';
import AppRouter from 'navigation/AppRouter';

const App = () => {
  return (
    <ErrorBoundary>
      <AppRouter />
    </ErrorBoundary>
  );
};

export default App;
