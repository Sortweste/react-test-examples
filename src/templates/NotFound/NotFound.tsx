import { Link } from 'react-router-dom';

import styles from './NotFound.module.scss';

const NotFound = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <h1>404 - Not Found!</h1>
      <Link to="/">HomePage</Link>
    </div>
  );
};

export default NotFound;
