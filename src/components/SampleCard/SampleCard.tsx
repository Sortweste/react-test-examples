import { MouseEvent, useState } from 'react';

import Button from 'components/Button';
import Card from 'components/Card';

import styles from './SampleCard.module.scss';

const SampleCard = () => {
  const [displayAdditionalInfo, setDisplayAdditionalInfo] = useState(false);

  const information = displayAdditionalInfo
    ? "This Pokémon's Speed is doubled during strong sunlight.\n\nThis bonus does not count as a stat modifier."
    : 'When this Pokémon has 1/3 or less of its HP remaining, its grass-type moves inflict 1.5× as much regular damage.';

  const onClick = (e: MouseEvent<HTMLButtonElement>): void =>
    setDisplayAdditionalInfo(!displayAdditionalInfo);

  return (
    <Card>
      <img
        src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
        alt="pokemon"
      />
      <p>Bulbasaur</p>
      <p>{information}</p>
      <Button text="More information" onClick={onClick} />
    </Card>
  );
};

export default SampleCard;
