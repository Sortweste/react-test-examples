import { PropsWithChildren } from 'react';

import styles from './Container.module.scss';

type ContainerPropT = {
  id?: string;
};

const Container = ({ id, children }: PropsWithChildren<ContainerPropT>) => {
  return (
    <div id={id} className={styles.container}>
      {children}
    </div>
  );
};

export default Container;
