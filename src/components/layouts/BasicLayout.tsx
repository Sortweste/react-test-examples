import { ReactNode } from 'react';

import Footer from 'components/Footer';
import Navbar from 'components/Navbar';

type BasicLayoutPropT = {
  children: ReactNode;
};

const BasicLayout = ({ children }: BasicLayoutPropT) => {
  return (
    <>
      <Navbar />
      {children}
      <Footer />
    </>
  );
};

export default BasicLayout;
