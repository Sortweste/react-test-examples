import { memo } from 'react';

import { Link } from 'react-router-dom';

import Routes from 'navigation/routes';

// import './Navbar.scss';

const Navbar = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link to={Routes.HOME}>Home</Link>
        </li>
        <li>
          <Link to={Routes.FORM}>Form</Link>
        </li>
        <li>
          <Link to={Routes.POKEDEX}>Pokedex</Link>
        </li>
      </ul>
    </nav>
  );
};

export default memo(Navbar);
