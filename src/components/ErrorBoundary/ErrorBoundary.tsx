import { Component, ErrorInfo, ReactNode } from 'react';

type State = {
  hasError: boolean;
  error: Error;
  info: ErrorInfo;
};

type Props = {
  children: ReactNode;
};

class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      hasError: false,
      error: { message: '', stack: '', name: '' },
      info: { componentStack: '' },
    };
  }

  static getDerivedStateFromError(): { hasError: boolean } {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    this.setState({ error, info: errorInfo });
  }

  render(): JSX.Element | ReactNode {
    const { hasError, error, info } = this.state;
    const children = this.props;
    if (hasError) {
      return (
        <div>
          <h1>Something went wrong!</h1>
          <p>{error.message}</p>
          <p>{info.componentStack}</p>
        </div>
      );
    }
    return children.children;
  }
}

export default ErrorBoundary;
