import { MouseEvent } from 'react';

import styles from './Button.module.scss';

export type ButtonPropT = {
  className?: string;
  disabled?: boolean;
  onClick: (e: MouseEvent<HTMLButtonElement>) => void;
  text: string;
};

const Button = ({ className, disabled, onClick, text }: ButtonPropT) => {
  return (
    <button
      disabled={disabled}
      type="button"
      onClick={onClick}
      className={className}
    >
      {text}
    </button>
  );
};

export default Button;
