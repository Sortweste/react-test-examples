import { PropsWithChildren } from 'react';

import classNames from 'classnames';

import styles from './Row.module.scss';

type RowPropT = {
  id?: string;
  justify?: 'center' | 'start' | 'end' | 'between';
  align?: 'center' | 'start' | 'end' | 'normal';
};

const Row = ({
  id,
  justify = 'center',
  align = 'normal',
  children,
}: PropsWithChildren<RowPropT>) => {
  const rowClassNames = classNames(styles.row, {
    [`${styles.row}_${justify}`]: justify,
    [`${styles.row}_align_${align}`]: align,
  });

  return (
    <div id={id} className={rowClassNames}>
      {children}
    </div>
  );
};

export default Row;
