import styles from './Spinner.module.scss';

const Spinner = (): JSX.Element => {
  return <div role="alert" className={styles.loader} />;
};

export default Spinner;
