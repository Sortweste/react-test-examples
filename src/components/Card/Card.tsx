import { ReactNode } from 'react';

type CardPropT = {
  children: ReactNode;
};

const Card = ({ children }: CardPropT) => {
  return <div>{children}</div>;
};

export default Card;
