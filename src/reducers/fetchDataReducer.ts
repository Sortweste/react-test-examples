import FetchDataActions from 'actions/fetchDataActions';
import FetchActionTypes from 'actions/fetchDataTypes';
import { IFetchState } from 'types/generics';

const fetchDataReducer = <T>(
  state: IFetchState<T>,
  action: FetchDataActions<T>
): IFetchState<T> => {
  switch (action.type) {
    case FetchActionTypes.FETCH_DATA_INIT:
      return {
        ...state,
        isLoading: true,
      };
    case FetchActionTypes.FETCH_DATA_SUCCESS:
      return {
        data: action.payload,
        isLoading: false,
        isError: false,
        isSuccess: true,
      };
    case FetchActionTypes.FETCH_DATA_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        isError: true,
        isSuccess: false,
      };
    default:
      return state;
  }
};

export default fetchDataReducer;
