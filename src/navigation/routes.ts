enum Routes {
  HOME = '/',
  FORM = '/form',
  POKEDEX = '/pokedex',
}

export default Routes;
