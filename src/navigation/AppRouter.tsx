import { lazy, Suspense } from 'react';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Routes from './routes';

const Home = lazy(() => import('pages/Home'));
const NotFound = lazy(() => import('pages/NotFound'));

const AppRouter = () => {
  return (
    <BrowserRouter>
      <Suspense fallback={<p>Loading...</p>}>
        <Switch>
          <Route exact path={Routes.HOME}>
            <Home />
          </Route>
          <Route exact path={Routes.FORM}>
            <Home />
          </Route>
          <Route exact path={Routes.POKEDEX}>
            <Home />
          </Route>
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
};

export default AppRouter;
