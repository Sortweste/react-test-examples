import { useReducer, useEffect, Reducer } from 'react';

import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  CancelTokenSource,
} from 'axios';

import FetchDataActions, {
  onFetchDataFailureCreator,
  onFetchDataInitCreator,
  onFetchDataSuccessCreator,
} from 'actions/fetchDataActions';
import fetchDataReducer from 'reducers/fetchDataReducer';
import { IFetchState, IResponse } from 'types/generics';

const initialState = {
  isLoading: false,
  isError: false,
  isSuccess: false,
  error: '',
};

const useAxios = <T>(config: AxiosRequestConfig): [IFetchState<T>] => {
  const [state, dispatch] = useReducer<
    Reducer<IFetchState<T>, FetchDataActions<T>>
  >(fetchDataReducer, initialState);

  const fetchData = async (source: CancelTokenSource) => {
    dispatch(onFetchDataInitCreator());
    try {
      const response: AxiosResponse<IResponse<T>> = await axios({
        ...config,
        cancelToken: source.token,
      });
      if (response.status === 200) {
        dispatch(onFetchDataSuccessCreator(response.data.data));
        return;
      }
      throw new Error(response.status.toString());
    } catch (err) {
      if (!axios.isCancel(err)) {
        if (err instanceof Error) {
          dispatch(onFetchDataFailureCreator(err.message));
        } else {
          throw err;
        }
      }
    }
  };

  useEffect(() => {
    const source = axios.CancelToken.source();
    fetchData(source);
    return () => {
      source.cancel();
    };
  }, [config]);

  return [state];
};

export default useAxios;
