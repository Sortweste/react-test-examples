import BasicLayout from 'components/layouts/BasicLayout';
import HomeTemplate from 'templates/Home';

const Home = () => {
  return (
    <BasicLayout>
      <HomeTemplate />
    </BasicLayout>
  );
};

export default Home;
