import BasicLayout from 'components/layouts/BasicLayout';
import NotFound from 'templates/NotFound';

const Home = () => {
  return (
    <BasicLayout>
      <NotFound />
    </BasicLayout>
  );
};

export default Home;
